# Contribution Guide
We appreciate your thought to contribute to open source. :heart: We want to make contributing as easy as possible. You are welcome to:

- Report a bug
- Discuss the current state of the code
- Submit a fix
- Propose new features

Merge requests are the best way to propose changes to the codebase. We actively welcome your pull requests:

1. Fork the repository and create your branch from `master` (the default branch).
2. Raise a merge request. We encourage you to fill out the predefined template.

## Pre-Commit
Use Pre-Commit on your local machine to validate your contribution on your local machine. The tool will detect
the most common errors and provides you with a solution.

Installation: `pre-commit install --hook-type=commit-msg`

The full documentation is available on the [Pre-Commit website](https://pre-commit.com/).

## Conventional Commits
We use Conventional Commits based on the Angular convention. But we don't use scopes.
For details see: https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines

The pipeline enforces this standard.

## License
By contributing, you agree that your contributions will be licensed under the GNU Affero General Public License.
