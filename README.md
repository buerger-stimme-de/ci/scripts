# Scripts

Useful scripts for pipelines and daily work.

## Command line
`source .user_bashrc` to get some aliases for Gitlab and Git.

## Set Gitlab Project Defaults
Export your personal `GITLAB_TOKEN` and run `set_gitlab_project_defaults.sh` to fix the projects settings
for all projects below our top level group `buerger-stimme-de`.

## Pipelines
Pipeline definitions are available in the `/pipeline` folder. In case of a standard pipeline
include the `global.yml` only.

## Job Templates
Templates for common jobs can be found in `/pipeline/job`.
