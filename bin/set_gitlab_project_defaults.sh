#!env bash
set -euo pipefail

#
# Updates all settings in all Gitlab and sets the defaults.
#
# $1: Project id, optional.
#
# GITLAB_TOKEN: a token which can be used to access Gitlab
#

top_level_group_id=15361268 # buerger-stimme-de

echo "Resetting project defaults recursively for group ${top_level_group_id}"

if [[ ! ${GITLAB_TOKEN:-} ]]; then
  echo "Set the GITLAB_TOKEN so the script can access Gitlab on your behalf."
  exit 2;
fi

authorization_header="Authorization: Bearer ${GITLAB_TOKEN}"
api_url="https://gitlab.com/api/v4"

project_global_defaults='auto_cancel_pending_pipelines=enabled&only_allow_merge_if_all_discussions_are_resolved=true&only_allow_merge_if_pipeline_succeeds=true&remove_source_branch_after_merge=true&merge_method=ff&build_timeout=3600&issues_access_level=enabled&wiki_access_level=enabled&snippets_access_level=disabled&requirements_access_level=enabled&pages_access_level=enabled&operations_access_level=enabled&container_registry_access_level=enabled&squash_option=always&auto_cancel_pending_pipelines=enabled&autoclose_referenced_issues=true&builds_access_level=enabled&keep_latest_artifact=true&lfs_enabled=false&merge_pipelines_enabled=true&packages_enabled=true&public_builds=true&restrict_user_defined_variables=true&shared_runners_enabled=true'
container_expiration_policy='{"container_expiration_policy_attributes": {"enabled": true, "keep_n": 10, "cadence": "7d", "older_than": "14d", "name_regex_delete": ".*", "name_regex_keep": "\d+.\d+.\d+"}}'
squash_commit_template="squash_commit_template=%{title}

%{description}

%{issues}
Merge request: %{reference}"

project_approval_default="reset_approvals_on_push=true&disable_overriding_approvers_per_merge_request=false&merge_requests_author_approval=false"
project_approval_team="name=Team&approvals_required=1&rule_type=regular&group_ids=${top_level_group_id}"

offset_project_id=0
project_ids=()
initial_do=1

# as long as 20 project are returned --> do it again with offset
while [ "$initial_do" -eq 1 ] || [ ${#project_ids[@]} -eq 20 ]
do
  # if project id given --> do not fetch all projects but use this id
  if [[ ! ${1:-} ]]; then
    project_ids=( $(curl --header "$authorization_header" "${api_url}/groups/${top_level_group_id}/projects?include_subgroups=true&simple=true&archived=false&id_after=$offset_project_id&sort=asc" | jq -r '.[].id') )
  else
    project_ids=($1)
  fi

  for id in "${project_ids[@]}"; do
    # remove whitespaces from id
    id=`echo "$id" | xargs`
    echo "Processing project $id"

    # fetch default branch
    default_branch=`curl --header "$authorization_header" -X GET --silent "${api_url}/projects/$id" | jq -r '.default_branch'`

    echo "Setting project defaults ..."
    curl -w "%{http_code}" --header "$authorization_header" -X PUT -d "${project_global_defaults}" "${api_url}/projects/$id"
    curl -w "%{http_code}" --header "$authorization_header" --header 'Content-Type: application/json;charset=UTF-8' -X PUT -d "${container_expiration_policy}" "${api_url}/projects/$id"
    curl -w "%{http_code}" --header "$authorization_header" -X PUT --data-urlencode "${squash_commit_template}" "${api_url}/projects/$id"

    echo "Setting protected branches ..."
    curl -w "%{http_code}" --header "$authorization_header" --request DELETE "${api_url}/projects/$id/protected_branches/master"
    curl -w "%{http_code}" --header "$authorization_header" --request DELETE "${api_url}/projects/$id/protected_branches/main"

    protected_default_branch="name=${default_branch}&merge_access_level=40&push_access_level=0"
    default_branch_id=`curl --header "$authorization_header" -X POST "${api_url}/projects/$id/protected_branches?${protected_default_branch}" | jq -r '.id'`

    echo "Setting approval defaults ..."
    curl -w "%{http_code}" --header "$authorization_header" -X POST "${api_url}/projects/$id/approvals?${project_approval_default}"

    rule_ids=( $(curl --header "$authorization_header" "${api_url}/projects/$id/approval_rules" | jq -r '.[].id') )

    for rule_id in "${rule_ids[@]}"; do
      rule_id=`echo "$rule_id" | xargs`
      echo "Deleting approval rule ($rule_id)"
      curl -w "%{http_code}" --header "$authorization_header" --request DELETE "${api_url}/projects/$id/approval_rules/$rule_id"
    done

    curl -w "%{http_code}" --header "$authorization_header" -X POST "${api_url}/projects/$id/approval_rules?${project_approval_team}&protected_branch_ids=${default_branch_id}"
  done

initial_do=0
offset_project_id=$id
done

#
# Special settings for the Renovate project: No public access to the pipelines as outlined
# in https://gitlab.com/renovate-bot/renovate-runner
#
# As a GITLAB_TOKEN and some others are required but those CI/CD variables are protected (set in protected branches only), merge_pipelines
# will never succeed in feature branches (unprotected).
#
echo "Setting project defaults for Renovate ..."
id=33573571
project_defaults='public_builds=false&builds_access_level=private&only_allow_merge_if_pipeline_succeeds=false'

curl -w "%{http_code}" --header "$authorization_header" -X PUT -d "${project_defaults}" "${api_url}/projects/$id"

#
# Set the group defaults. Create the labels manually. We do an update only
#

echo "Updating group labels ..."
label_id=23844898
curl -w "%{http_code}" --header "Content-Type: application/json" --header "$authorization_header" \
  --data '{"new_name": "dependencies", "color": "#ed9121", "description": "automatic dependency updates by Renovate"}' \
  --request PUT "${api_url}/groups/$top_level_group_id/labels/$label_id"

label_id=23844874
curl -w "%{http_code}" --header "Content-Type: application/json" --header "$authorization_header" \
  --data '{"new_name": "follow up", "color": "#0000ff", "description": "things still to do which were not included in a previously merged merge request"}' \
  --request PUT "${api_url}/groups/$top_level_group_id/labels/$label_id"
